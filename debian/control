Source: golang-github-muesli-cancelreader
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Martin Dosch <martin@mdosch.de>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-golang-x-sys-dev
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-muesli-cancelreader
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-muesli-cancelreader.git
Homepage: https://github.com/muesli/cancelreader
XS-Go-Import-Path: github.com/muesli/cancelreader

Package: golang-github-muesli-cancelreader-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-golang-x-sys-dev,
         ${misc:Depends}
Description: Cancelable reader for Go (library)
 This package is based on the fantastic work of Erik Geiser
 (https://github.com/erikgeiser) in Charm's Bubble Tea
 (https://github.com/charmbracelet/bubbletea) framework.
 .
 NewReader returns a reader with a Cancel function. If the input reader
 is a File, the cancel function can be used to interrupt a blocking Read
 call. In this case, the cancel function returns true if the call was
 canceled successfully. If the input reader is not a File, the cancel
 function does nothing and always returns false.
